## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install python deps.

```bash
pip3 install -r requirements.txt
```

## Usage

```python
python3 crawler.py [URL]
```
OR
```python
chmod +x crawler.py
./crawler.py [URL]
```



## License

[MIT](https://choosealicense.com/licenses/mit/)