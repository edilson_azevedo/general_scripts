import requests
from bs4 import BeautifulSoup
from urllib.parse import urljoin
from collections import deque
import sys

def is_valid_url(url):
    """Check if a URL is valid and can be crawled."""
    if url.startswith("mailto:"):
        return False
    return True

def get_links(url):
    """Fetch all links from a given URL."""
    links = set()
    try:
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')
        for link in soup.find_all('a', href=True):
            if is_valid_url(link['href']):
                full_url = urljoin(url, link['href'])
                links.add(full_url)
    except requests.exceptions.RequestException as e:
        print(f"Request failed: {e}")
    return links

def search_smartreaders(url, visited, to_visit):
    """Crawl a website for smartreaders and follow all links."""
    print(f"Visiting: {url}")
    try:
        response = requests.get(url)
        # Define logic to search for "smartreaders" here
        if "smartreader" in response.text.lower():
            print(f"Smartreader found at: {url}")

        visited.add(url)
        links = get_links(url)
        for link in links:
            if link not in visited:
                to_visit.append(link)
    except requests.exceptions.RequestException as e:
        print(f"Request failed: {e}")

def crawl(start_url):
    """Crawl from a start URL and look for smartreaders."""
    visited = set()
    to_visit = deque([start_url])
    
    while to_visit:
        current_url = to_visit.popleft()
        if current_url not in visited:
            search_smartreaders(current_url, visited, to_visit)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        start_url = sys.argv[1]  # Define URL as argument
        crawl(start_url)
    else:
        print("Usage: python script.py <URL>")
        sys.exit(1)
